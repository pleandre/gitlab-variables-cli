package connector

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"apilab.dev/gitlab-variables-cli/src/settings"
)

// Variable returned by GitLab API
type Variable struct {
	Key   string `json:"key"`
	Type  string `json:"variable_type"`
	Value string `json:"value"`
}

func get(url string, config settings.Settings) []Variable {
	// Create request
	req, err := http.NewRequest("GET", config.GetProjectVariablesAPIURL(), nil)
	req.Header.Set("PRIVATE-TOKEN", config.PrivateToken)
	if err != nil {
		panic(err)
	}

	// Send
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	// Read response
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}

	// Deserialize
	var variables []Variable
	err = json.Unmarshal(body, &variables)
	if err != nil {
		panic(fmt.Sprintf("%+v\n%s", err, body))
	}

	return variables
}

// GetVariables returns the list of variables for a GitLab config: group variables first then project variables
func GetVariables(config settings.Settings) []Variable {
	projectVariables, groupVariables := []Variable{}, []Variable{}

	if config.HasGroup() {
		groupVariables = get(config.GetGroupVariablesAPIURL(), config)
	}

	if config.HasProject() {
		projectVariables = get(config.GetProjectVariablesAPIURL(), config)
	}

	// Concatenate variables: group variables then project variables
	return append(groupVariables, projectVariables...)
}
