package main

import (
	"flag"
	"fmt"
	"os"

	"apilab.dev/gitlab-variables-cli/src/connector"
	"apilab.dev/gitlab-variables-cli/src/environment"
	"apilab.dev/gitlab-variables-cli/src/settings"
)

func error(message string) {
	fmt.Fprintf(os.Stderr, "Error: %s\nA token and a project or a group need to be set.\nUsage of %s:\n", message, os.Args[0])
	flag.PrintDefaults()
	os.Exit(1)
}

func main() {
	config := settings.GetSettings()

	if config.PrivateToken == "" {
		error("please provide a gitlab private token (environment variable PRIVATE_TOKEN or command line flag privateToken)")
	}

	if !config.HasProject() && !config.HasGroup() {
		error("error: please provide a project path, a project id or group path or group id (in the environment variables or as a command line flag)")
	}

	variables := connector.GetVariables(config)
	environment.SetVariables(variables)

	os.Exit(0)
}
