package environment

import (
	"io/ioutil"
	"os"

	"apilab.dev/gitlab-variables-cli/src/connector"
)

func createFile(variable connector.Variable) string {
	tmpfile, err := ioutil.TempFile("", "gitlab_var_")
	if err != nil {
		panic(err)
	}

	if _, err := tmpfile.Write([]byte(variable.Value)); err != nil {
		panic(err)
	}

	if err := tmpfile.Close(); err != nil {
		panic(err)
	}

	return tmpfile.Name()
}

// SetVariables creates environment variables and files
func SetVariables(variables []connector.Variable) {
	for _, variable := range variables {

		value := variable.Value
		if variable.Type == "file" {
			value = createFile(variable)
		}

		os.Setenv(variable.Key, value)
	}
}
