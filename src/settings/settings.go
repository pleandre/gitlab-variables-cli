package settings

import (
	"flag"
	"fmt"
	"net/url"
	"os"
	"strconv"
)

// Settings struct
type Settings struct {
	BaseURL      string
	PrivateToken string
	ProjectID    int
	ProjectPath  string
	GroupID      int
	GroupPath    string
}

func getString(p *string, environmentVariable string, flagName string, defaultValue string, usage string) {
	*p = os.Getenv(environmentVariable)
	if *p != "" {
		defaultValue = *p
	}

	flag.StringVar(p, flagName, defaultValue, fmt.Sprintf("%s\nCan also be set with environment variable `%s`", usage, environmentVariable))
}

func getInt(p *int, environmentVariable string, flagName string, defaultValue int, usage string) {
	result := os.Getenv(environmentVariable)
	if result != "" {
		defaultValue = *p
	} else if i, err := strconv.Atoi(result); err == nil {
		defaultValue = i
	}

	flag.IntVar(p, flagName, defaultValue, fmt.Sprintf("%s\nCan also be set with environment variable `%s`", usage, environmentVariable))
}

// GetSettings returns our application settings, settings are parsed from environment variables or flags
func GetSettings() Settings {
	var settings Settings
	getString(&settings.BaseURL, "API_V4_URL", "baseURL", "https://gitlab.com/api/v4", "GitLab API base URL")
	getString(&settings.PrivateToken, "PRIVATE_TOKEN", "privateToken", "", "(Required) GitLab private token")
	getInt(&settings.ProjectID, "PROJECT_ID", "projectID", 0, "GitLab ID of a project owned by the authenticated user")
	getString(&settings.ProjectPath, "PROJECT_PATH", "projectPath", "", "GitLab project path (e.g. NAMESPACE/PROJECT_NAME) of a project owned by the authenticated user")
	getInt(&settings.GroupID, "GROUP_ID", "groupID", 0, "GitLab ID of a group owned by the authenticated user")
	getString(&settings.GroupPath, "GROUP_PATH", "groupPath", "", "GitLab path of a group owned by the authenticated user")

	flag.Parse()
	return settings
}

// GetProjectVariablesAPIURL returns GitLab Project Variable API Url based on the settings
func (s Settings) GetProjectVariablesAPIURL() string {
	id := strconv.Itoa(s.ProjectID)
	if id == "0" {
		id = url.QueryEscape(s.ProjectPath)
	}

	return fmt.Sprintf("%s/projects/%s/variables", s.BaseURL, id)
}

// GetGroupVariablesAPIURL returns GitLab Project Variable API Url based on the settings
func (s Settings) GetGroupVariablesAPIURL() string {
	id := strconv.Itoa(s.ProjectID)
	if id == "0" {
		id = url.QueryEscape(s.ProjectPath)
	}

	return fmt.Sprintf("%s/groups/%s/variables", s.BaseURL, id)
}

// HasProject returns wheter or not a project is setup in the settings. Either a project ID or a project path needs to be configured
func (s Settings) HasProject() bool {
	if s.ProjectPath == "" && s.ProjectID == 0 {
		return false
	}

	return true
}

// HasGroup returns wheter or not a group is setup in the settings. Either a group ID or a group path needs to be configured
func (s Settings) HasGroup() bool {
	if s.GroupPath == "" && s.GroupID == 0 {
		return false
	}

	return true
}
